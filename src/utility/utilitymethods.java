package utility;

public class utilitymethods {
    public static double convertWordToNumbers(String numberinwords)
    {
        double bignumber;
        if (numberinwords.contains("k")){
            bignumber = (Double.parseDouble(numberinwords.replaceFirst(".$", "")))*1000;
        }
        else if (numberinwords.contains("M")){
            bignumber = (Double.parseDouble(numberinwords.replaceFirst(".$", "")))*1000000;
        } else if (numberinwords.contains("B")) {
            bignumber = (Double.parseDouble(numberinwords.replaceFirst(".$", "")))*1000000000;
        } else {
            bignumber = (Double.parseDouble(numberinwords));
        }
        return  bignumber;
    }
}
