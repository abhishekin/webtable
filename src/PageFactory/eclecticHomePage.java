package PageFactory;

import com.google.common.collect.Ordering;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.SkipException;

import java.util.ArrayList;
import java.util.List;

import static utility.utilitymethods.convertWordToNumbers;


public class eclecticHomePage {

	/**
	 * All WebElements are identified by @FindBy annotation
	 */
	WebDriver driver;

	@FindBy(name="filter-input")
	WebElement filterdata;

	@FindBy(xpath="//div[@class=\"table-row\"]")
	List<WebElement> filterdataList;

	@FindBy(name="sort-select")
	WebElement sortdata;


	public eclecticHomePage(WebDriver driver){
		this.driver = driver;
		//This initElements method will create  all WebElements
		PageFactory.initElements(driver, this);
	}

	public void enterFilterData(String filterby){
		Reporter.log("Clearing filter box",true);
		filterdata.clear();
		Reporter.log("Entering filer data: " + filterby,true);
		filterdata.sendKeys(filterby);
	}

	public void validateFiltering(String filterby)
	{
		if (filterdataList.size()==0){
			Reporter.log( "No data rows are available for filer: " + filterby, true);
			throw new SkipException("Skipping tests because No data rows are available for filer: " + filterby);
		} else {
			Reporter.log("Filtered rows are available, Validating data..",true);
			for (WebElement filterdata : filterdataList) {
				Assert.assertTrue((filterdata.getText().toLowerCase()).contains(filterby));
			}
			Reporter.log("Clearing filter box",true);
			filterdata.clear();
			Reporter.log("Filtered rows are available, Validated data..",true);
		}
	}

	public void enterSortingData(String sortby){
		Select dropdown = new Select(sortdata);
		Reporter.log("Selecting sortby: " + sortby,true);
		dropdown.selectByValue(sortby);
	}


	public void validateSorting(String sortby) throws InterruptedException {
		ArrayList<Double> obtainedList = new ArrayList<>();
		List<WebElement> elementList = driver.findElements(By.xpath("//*[contains(@class,'" + sortby + "')]"));
		if (elementList.size() == 0) {
			Reporter.log("No data rows are available for Sort: " + sortby, true);
			throw new SkipException("Skipping tests because No data rows are available for Sort: " + sortby);
		} else {
			Reporter.log("Sorted rows are available, Validating data..",true);
			for (WebElement we : elementList) {
				double d = convertWordToNumbers(we.getText());
				obtainedList.add(d);
			}
			boolean sorted = Ordering.natural().isOrdered(obtainedList);
			Assert.assertTrue(sorted, "List is not Sorted" + obtainedList);
			Reporter.log("Sorted rows are available, Validated data..",true);
		}
	}
}
