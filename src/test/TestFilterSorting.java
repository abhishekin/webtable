package test;

import PageFactory.eclecticHomePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


import java.util.concurrent.TimeUnit;

public class TestFilterSorting {

    WebDriver driver;
    eclecticHomePage objeclecticHomePage;

    @BeforeTest
    public void setup(){
        System.setProperty("webdriver.chrome.driver", "driver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://mystifying-beaver-ee03b5.netlify.app/");
    }


    @Test(priority=0)
    public void test_FilterSortingValidData1() throws InterruptedException {
        //Create Login Page object
        Reporter.log("Running Tests: test_FilterSortingValidData1" , true);
        objeclecticHomePage = new eclecticHomePage(driver);
        objeclecticHomePage.enterFilterData("w");
        objeclecticHomePage.enterSortingData("cases");
        objeclecticHomePage.validateFiltering("w");
        objeclecticHomePage.validateSorting("cases");
        Reporter.log("Test Completed: test_FilterSortingValidData1" , true);
    }

    @Test(priority=1)
    public void test_FilterSortingValidData2() throws InterruptedException {
        //Create Login Page object
        Reporter.log("Running Tests: test_FilterSortingValidData2" , true);
        objeclecticHomePage = new eclecticHomePage(driver);
        objeclecticHomePage.enterFilterData("a");
        objeclecticHomePage.enterSortingData("averageImpact");
        objeclecticHomePage.validateFiltering("a");
        objeclecticHomePage.validateSorting("averageImpact");
        Reporter.log("Test Completed: test_FilterSortingValidData2" , true);

    }

    @Test(priority=2)
    public void test_FilterSortingInValidData1() throws InterruptedException {
        //Create Login Page object
        Reporter.log("Running Tests: test_FilterSortingInValidData1" , true);
        objeclecticHomePage = new eclecticHomePage(driver);
        objeclecticHomePage.enterFilterData("abc");
        objeclecticHomePage.enterSortingData("cases");
        objeclecticHomePage.validateFiltering("abc");
        objeclecticHomePage.validateSorting("cases");
        Reporter.log("Test Completed: test_FilterSortingInValidData1" , true);
    }

    @Test(priority=3)
    public void test_FilterOnly() throws InterruptedException {
        //Create Login Page object
        Reporter.log("Running Tests: test_FilterOnly" , true);
        objeclecticHomePage = new eclecticHomePage(driver);
        objeclecticHomePage.enterFilterData("i");
        objeclecticHomePage.validateFiltering("i");
        Reporter.log("Test Completed: test_FilterOnly" , true);
    }

    @Test(priority=4)
    public void test_SortingOnly() throws InterruptedException {
        //Create Login Page object
        Reporter.log("Running Tests: test_SortingOnly" , true);
        objeclecticHomePage = new eclecticHomePage(driver);
        objeclecticHomePage.enterSortingData("cases");
        objeclecticHomePage.validateSorting("cases");
        Reporter.log("Test Completed: test_SortingOnly" , true);
    }


    @AfterTest
    public void teardown(){
        driver.close();
    }
}

