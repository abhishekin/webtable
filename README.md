# README #
How to Run ?

1. Install Maven, TestNG


2. Clone the repository to any IDE supporting Java code


3. For Intellij, Right click on POM file and select 'Add as Maven Project'

4. Ideally Maven should download all the dependencies automatically , incase its not , You may rightclick on Project, Select Maven and download the resources. 

5. Right click on suite/RunAllTests.xml 


 For Linux , The process would be same if Maven and testNG is installed.

